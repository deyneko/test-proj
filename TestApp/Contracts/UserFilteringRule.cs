﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApp.Contracts
{
    public class UserFilteringRule
    {
        public string FilterFieldName { get; set; }
        public string FilterFieldValue { get; set; }
    }
}
