﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApp.Attributes;
using TestApp.BL;
using TestApp.Contracts;

namespace TestApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {

        private readonly ILogger<UserController> _logger;
        private readonly IUsersManager _usersMagager;

        public UserController(ILogger<UserController> logger, IUsersManager usersMagager)
        {
            _usersMagager = usersMagager == null ? throw new ArgumentNullException(nameof(usersMagager)) : usersMagager;
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<UserDetails> Find(IEnumerable<UserFilteringRule> filteringRules)
        {
            //Validate Names
            var filteredUsers = _usersMagager.FindUsersForRules(filteringRules);
            return filteredUsers;
        }

        [HttpPost]
        [ServiceFilter(typeof(RestrictByIpActionFilter))]
        public void Add(UserDetails user)
        {
            _usersMagager.AddNewUser(user);
        }

        [HttpPut]
        public void Update(UserDetails user)
        {
            _usersMagager.UpdateUser(user);
        }

    }
}
