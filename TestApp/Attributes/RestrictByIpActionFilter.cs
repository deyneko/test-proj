﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Net;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using TestApp.BL;
using System;

namespace TestApp.Attributes
{
    public class RestrictByIpActionFilter : ActionFilterAttribute
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IIpService _ipService;

        public RestrictByIpActionFilter(IConfiguration configuration, IIpService ipService, ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<RestrictByIpActionFilter>();
            _ipService = ipService;
            _configuration = configuration;
        }

        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {

            if (string.IsNullOrWhiteSpace(_configuration["AllowedCoutryCodesCSV"]))
            {
                throw new InvalidOperationException("Mossing AllowedCoutryCodesCSV configuration");
            }

            var remoteIp = context.HttpContext.Connection.RemoteIpAddress;
            _logger.LogDebug("Remote IpAddress: {RemoteIp}", remoteIp);

            if (remoteIp.IsIPv4MappedToIPv6)
            {
                remoteIp = remoteIp.MapToIPv4();
            }

            if (await _ipService.IsIpAllowed(_configuration["AllowedCoutryCodesCSV"], remoteIp.ToString()))
            {
                await next();
            }
            else            
            {
                _logger.LogWarning("Forbidden Request from IP: {RemoteIp}", remoteIp);
                context.Result = new StatusCodeResult(StatusCodes.Status403Forbidden);
                return;
            }
        }
    }
}
