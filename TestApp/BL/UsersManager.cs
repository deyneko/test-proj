﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.Json;
using TestApp.Contracts;
using TestApp.DB;
using TestApp.DB.Models;

namespace TestApp.BL
{
    public class UsersManager : IUsersManager
    {
        private readonly MainDbContext _mainDbContext;
        private readonly IQueueClient _queueClient;
        private Mapper _mapper;
        public UsersManager(MainDbContext mainDbContext, IQueueClient queueClient)
        {
            _mainDbContext = mainDbContext;
            _queueClient = queueClient;
            InitMapper();
        }

        private void InitMapper()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<UserDetails, User>().ReverseMap());
            _mapper = new Mapper(config);
        }

        public void AddNewUser(UserDetails newUser)
        {
            var userInDb = _mapper.Map<User>(newUser);
            _mainDbContext.Users.Add(userInDb);
            _mainDbContext.SaveChanges();

            _queueClient.PushUserUptedNotification(JsonSerializer.Serialize(userInDb));
        }

        public void UpdateUser(UserDetails userUpdateDetails)
        {
            // Let's assume Email is unique
            var userInDb = _mainDbContext.Users.SingleOrDefault( user=> user.Email.ToLower() == userUpdateDetails.Email.ToLower());

            if (userInDb==null)
            {
                throw new InvalidOperationException( $"User for {userUpdateDetails.Email} email is not existing" );
            }

            // Update only if etem is present
            userInDb.Address = string.IsNullOrWhiteSpace(userUpdateDetails.Address) ? userInDb.Address : userUpdateDetails.Address;
            userInDb.FirstName = string.IsNullOrWhiteSpace(userUpdateDetails.FirstName) ? userInDb.FirstName : userUpdateDetails.FirstName;
            userInDb.Password = string.IsNullOrWhiteSpace(userUpdateDetails.Password) ? userInDb.Password : userUpdateDetails.Password;

            _mainDbContext.SaveChanges();

            _queueClient.PushUserUptedNotification(JsonSerializer.Serialize(userInDb));
        }

        public List<UserDetails> FindUsersForRules(IEnumerable<UserFilteringRule> filteringRules)
        {
            if (filteringRules == null || !filteringRules.Any())
            {
                return _mapper.Map<List<UserDetails>>(_mainDbContext.Users);
            }

            var filteredUsers = from user in _mainDbContext.Users
                                where 
                                   (IsExemptFromFiltering(nameof(user.Address), filteringRules) || user.Address.ToLower().Contains(GetFilteringValue(nameof(user.Address), filteringRules)))
                                & (IsExemptFromFiltering(nameof(user.Email), filteringRules) || user.Email.ToLower().Contains(GetFilteringValue(nameof(user.Email), filteringRules)))
                                & (IsExemptFromFiltering(nameof(user.FirstName), filteringRules) || user.FirstName.ToLower().Contains(GetFilteringValue(nameof(user.FirstName), filteringRules)))
                                & (IsExemptFromFiltering(nameof(user.LastName), filteringRules) || user.LastName.ToLower().Contains(GetFilteringValue(nameof(user.LastName), filteringRules)))
                                select user;

            return _mapper.Map<List<UserDetails>>(filteredUsers);
        }

        private string GetFilteringValue(string propertyName, IEnumerable<UserFilteringRule> filteringRules)
        {
            return filteringRules.FirstOrDefault(fr => fr.FilterFieldName == propertyName).FilterFieldValue.ToLower();
        }

        private bool IsExemptFromFiltering(string propertyName, IEnumerable<UserFilteringRule> filteringRules)
        {
            return !filteringRules.Any(fr => fr.FilterFieldName == propertyName);
        }
    }
}
