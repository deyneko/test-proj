﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace TestApp.BL
{
    public class IpApiClient : IIpService
    {
        private const string IpApiBaseUrl = "https://ipapi.co/";
        public async  Task<bool> IsIpAllowed(string allowedCoutryCodesCSV, string remoteIp) 
        {
            using var client = new HttpClient();

#if DEBUG 
            //Use some SwissBank's ip for debug
            var request = new HttpRequestMessage(HttpMethod.Get, $"{IpApiBaseUrl}128.127.55.2/country");
#else
            var request = new HttpRequestMessage(HttpMethod.Get, $"{IpApiBaseUrl}{remoteIp}/country");
#endif
            request.Headers.Add("User-Agent", "ipapi.co/#c-sharp-v1.01");
            var result = await client.SendAsync(request);
            if (!result.IsSuccessStatusCode)
            {
                //By defauld if IpApi is down allow everyone
                return true;
            }

            var response = await result.Content.ReadAsStringAsync();
            return allowedCoutryCodesCSV.ToLower().Split(",".ToCharArray()).Contains(response.ToLower());
        }
    }
}
