﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.BL
{
    public class RabbitMqQueueClient : IQueueClient
    {
        private readonly IConfiguration _configuration;
        public RabbitMqQueueClient(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public void PushUserCreatedNotification(string msgJson) => PushNotification(msgJson, IQueueClient.UserCreatedQueue);

        public void PushUserUptedNotification(string msgJson) => PushNotification(msgJson, IQueueClient.UserUpdatedQueue);

        private void PushNotification(string msgJson, string queuName)
        {
            var factory = new ConnectionFactory() { HostName = _configuration.GetConnectionString("RabbitMQ") };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.ExchangeDeclare(exchange: IQueueClient.UsersExchange, type: ExchangeType.Direct);

            var body = Encoding.UTF8.GetBytes(msgJson);

            channel.BasicPublish(exchange: IQueueClient.UsersExchange,
                                 routingKey: queuName,
                                 basicProperties: null,
                                 body: body);
        }
    }
}
