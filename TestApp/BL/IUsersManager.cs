﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApp.Contracts;

namespace TestApp.BL
{
    public interface IUsersManager
    {
        public void AddNewUser(UserDetails newUser);
        public void UpdateUser(UserDetails userUpdateDetails);
        public List<UserDetails> FindUsersForRules(IEnumerable<UserFilteringRule> filteringRules);
    }
}
