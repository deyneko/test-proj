﻿using System.Threading.Tasks;

namespace TestApp.BL
{
    public interface IIpService
    {
        public Task<bool> IsIpAllowed(string allowedCoutryCodesCSV, string remoteIp);
    }
}