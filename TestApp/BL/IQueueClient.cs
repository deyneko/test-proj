﻿namespace TestApp.BL
{
    public interface IQueueClient
    {
        public const string UserCreatedQueue = "UserCreatedQueue";
        public const string UserUpdatedQueue = "UserUpdatedQueue";
        public const string UsersExchange = "UsersExchange";
        public void PushUserCreatedNotification(string msgJson);
        public void PushUserUptedNotification(string msgJson);
    }
}