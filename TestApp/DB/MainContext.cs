﻿using Microsoft.EntityFrameworkCore;
using System.Configuration;
using TestApp.DB.Models;

namespace TestApp.DB
{
    public class MainDbContext : DbContext
    {
        public virtual DbSet<User> Users { get; set; }

        public MainDbContext()
        {
        }

        public MainDbContext(DbContextOptions<MainDbContext> options)
        : base(options)
        {
        }
    }
}
