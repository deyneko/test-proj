using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using TestApp.BL;
using TestApp.Contracts;
using TestApp.DB;
using TestApp.DB.Models;

namespace TestApp.Test
{
    public class UsersManagerUnitTest
    {
        [Test]
        public void Test_UsersFiltering()
        {
            //Prepare
            var testUsers = new List<User>
            {
                new User { Email = "First@email.com", LastName = "TestLN"},
                new User { Email = "Second@email.com" , LastName = "TestLN"}
            }.AsQueryable();

            var testFilters = new List<UserFilteringRule>
            {
                new UserFilteringRule { FilterFieldName  = nameof(User.LastName), FilterFieldValue = "TestLN" },
                new UserFilteringRule { FilterFieldName  = nameof(User.Email), FilterFieldValue = "First" }
            };

            var mockDbSet = new Mock<DbSet<User>>();
            mockDbSet.As<IQueryable<User>>().Setup(m => m.Provider).Returns(testUsers.Provider);
            mockDbSet.As<IQueryable<User>>().Setup(m => m.Expression).Returns(testUsers.Expression);
            mockDbSet.As<IQueryable<User>>().Setup(m => m.ElementType).Returns(testUsers.ElementType);
            mockDbSet.As<IQueryable<User>>().Setup(m => m.GetEnumerator()).Returns(testUsers.GetEnumerator());


            var mockContext = new Mock<MainDbContext>();
            mockContext.Setup(m => m.Users).Returns(mockDbSet.Object);
            var queueClient = new Mock<IQueueClient>();

            //Run
            var usersManager = new UsersManager(mockContext.Object, queueClient.Object);
            var filteredUsers = usersManager.FindUsersForRules(testFilters);


            //Check
            Assert.AreEqual(1, filteredUsers.Count);
            Assert.AreEqual("First@email.com", filteredUsers[0].Email);
            Assert.AreEqual("TestLN", filteredUsers[0].LastName);

        }
    }
}